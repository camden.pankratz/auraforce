({
	createCase : function(component, case1) { 
        console.log("helper has been reached"); //yes, this was reached in debugging
        
        //using the get-process-set pattern
        //the get
        let action = component.get("c.saveCase");
        
        //the process
        action.setParams({
            "case1" : case1
        });
        //the set
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS") {
                let cases = component.get("v.cases");
                cases.push(response.getReturnValue());
                component.set("v.cases", cases);
            }
        });
        $A.enqueueAction(action);                
	},
})