({
	handleSubmit : function(component, event, helper) {
        //console.log("does this even run?")
              
        //Grabbing all fields from inputform
        let caseFormName = component.find("caseFormName").get("v.value");
        let caseFormEmail = component.find("caseFormEmail").get("v.value");
        let caseFormVesselName = component.find("caseFormVesselName").get("v.value");
        let caseFormDescription = component.find("caseFormDescription").get("v.value");
        
        //setting newCase attribute
       	component.set("v.newCase.Name__c", caseFormName);
        component.set("v.newCase.Email", caseFormEmail);
        component.set("v.newCase.Vessel_Name__c", caseFormVesselName);
        component.set("v.newCase.Description", caseFormDescription);
        //console.log("newCase has been assigned" + component.get("v.newCase"));
        
        //assign to newCase attribute
        let newCase = component.get("v.newCase");
        
        
        //Send newCase to helper
        helper.createCase(component, newCase); //calling the helper
	
	},
    
})