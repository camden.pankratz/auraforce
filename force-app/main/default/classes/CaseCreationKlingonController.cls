public with sharing class CaseCreationKlingonController {

    public static String description;
    
    @AuraEnabled
    public static Case saveCase(Case case1) {
        //perform isUpdateable() checking first, then
        
        description=case1.Description;
        String translation = klingonTranslation(description);
        
        case1.Klingon_Description__c = translation;
        
        upsert case1;
        return case1;
    }
       
    @AuraEnabled
    public static String klingonTranslation(String englishInput) {
        System.debug('Accessed vesselSearch in Apex Controller');
        
        //pass in inputed description for translation
        //Build PARAMETERS:
        
        String xRapidApiHost = 'klingon.p.rapidapi.com';
        String xRapidApiKey = '977e7047b4msh14b836f8c8c5d60p1e1dffjsn15c0264c3315';
        
        //endpoint must look like: "https://klingon.p.rapidapi.com/klingon?text=I+will+terminate+you."
        englishInput = englishInput.replaceAll(' ', '+'); //might be '%20' rather than '+'
        System.debug(englishInput);
        String endpoint = 'https://'+ xRapidApiHost + '/klingon?text=' + englishInput;
        System.debug(endpoint); 
        
        //Callout to the API
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setHeader('x-rapidapi-host', xRapidApiHost);
        request.setHeader('x-rapidapi-key', xRapidApiKey);        
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = h.send(request);
        System.debug(response); //this worked as of 1239pm 10/26/2020
       
        //Parsing/Translating the JSON return
        String klingonOutput;
        Map<String,Object> translation = new Map<String,Object>();
        
        if(response.getStatus() == 'OK'){
            translation = (Map<String,Object>)JSON.deserializeUntyped(response.getBody());
            System.debug(translation);
            Map<String, Object> contentsMap = (Map<String, Object>)translation.get('contents');
            klingonOutput = String.valueOf(contentsMap.get('translated'));
            System.debug(klingonOutput); //this worked as of 154PM 10/26/2020
        } else {
            System.debug('could not retrieve transalation');
        }
        
        //write translation to the Attribute via return:
    	return klingonOutput;
	}    
}