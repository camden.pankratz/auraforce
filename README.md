# Auraforce

## Project Description

Auraforce is a custom project that leverages lightning communities, process automation, service cloud, sales cloud, and the lightning aura framework to fulfill the requirements of creating an interactive experience that consumes an external rest API.

## Technologies Used

-Lightning Component Framework (Aura Framework).
-Consuming an external API.
-Service cloud implementation.

## Features

List of features implemented:
Custom Web-to-case form made with Aura Components,
External user accessible,
Translates English Strings into Klingon via external API.

To-do list:
Full implementation of Sales Cloud complete with pricebooks and products,
Incorporating Klingon as another language option for the community.

## Getting Started

Clone: git clone https://gitlab.com/camden.pankratz/auraforce

## Usage

This project is intended to implenment a external accessed community. 

## License

This project used a API liscense given with a subscription to the Rakuten RapidAPI marketplace.
It can be viewed here: https://english.api.rakuten.net/orthosie/api/klingon-translator/details 
